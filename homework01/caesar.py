def encrypt_caesar(plaintext):
    """
    >>> encrypt_caesar("PYTHON")
    'SBWKRQ'
    >>> encrypt_caesar("python")
    'sbwkrq'
    >>> encrypt_caesar("")
    ''
    """
    ciphertext = ""
    for symbol in plaintext:
        if symbol.isalpha():
            if ord(symbol) >= ord('a'):
                ciphertext += chr((ord(symbol) - ord('a') + 3) % 26 + ord('a'))
            else:
                ciphertext += chr((ord(symbol) - ord('A') + 3) % 26 + ord('A'))
        elif ord(symbol) == ord(' '):
            ciphertext += chr(ord(' '))
        else:
            ciphertext += symbol
    return ciphertext


def decrypt_caesar(ciphertext):
    """
    >>> decrypt_caesar("SBWKRQ")
    'PYTHON'
    >>> decrypt_caesar("sbwkrq")
    'python'
    >>> decrypt_caesar("")
    ''
    """
    plaintext = ""
    for symbol in ciphertext:
        if symbol.isalpha():
            if ord(symbol) >= ord('a'):
                plaintext += chr((ord(symbol) - ord('a') + 23) % 26 + ord('a'))
            else:
                plaintext += chr((ord(symbol) - ord('A') + 23) % 26 + ord('A'))
        elif ord(symbol) == ord(' '):
            plaintext += chr(ord(' '))
        else:
            plaintext += symbol
    return plaintext
