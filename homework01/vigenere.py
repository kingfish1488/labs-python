def encrypt_vigenere(plaintext, keyword):
    """
    >>> encrypt_vigenere("PYTHON", "A")
    'PYTHON'
    >>> encrypt_vigenere("python", "a")
    'python'
    >>> encrypt_vigenere("ATTACKATDAWN", "LEMON")
    'LXFOPVEFRNHR'
    """
    ciphertext = []
    key_index = 0
    keyword.upper()
    abc = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

    for symbol in plaintext:
        num = abc.find(symbol.upper())
        if num != -1:
            num += abc.find(keyword[key_index].upper())
            num = num % 26
            if symbol.isupper():
                ciphertext.append(abc[num])
            elif symbol.islower():
                ciphertext.append(abc[num].lower())
            key_index += 1
            if key_index == len(keyword):
                key_index = 0
        else:
            ciphertext.append(symbol)
    return ''.join(ciphertext)


def decrypt_vigenere(ciphertext, keyword):
    """
    >>> decrypt_vigenere("PYTHON", "A")
    'PYTHON'
    >>> decrypt_vigenere("python", "a")
    'python'
    >>> decrypt_vigenere("LXFOPVEFRNHR", "LEMON")
    'ATTACKATDAWN'
    """
    plaintext = []
    key_index = 0
    keyword.upper()
    abc = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

    for symbol in ciphertext:
        num = abc.find(symbol.upper())
        if num != -1:
            num -= abc.find(keyword[key_index].upper())
            num = num % 26
            if symbol.isupper():
                plaintext.append(abc[num])
            elif symbol.islower():
                plaintext.append(abc[num].lower())
            key_index += 1
            if key_index == len(keyword):
                key_index = 0
        else:
            plaintext.append(symbol)
    return ''.join(plaintext)
